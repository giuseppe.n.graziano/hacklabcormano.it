// wifi
#include <ESP8266WiFi.h>
WiFiClient espClient;
IPAddress gateway;
byte mac[6];  // TODO: verificare dove lo usiamo
String ssid = "HackLabCormano";
String password = ""; // INSERIRE PASSWORD
#define WIFI_TENTATIVI 100
#define USE_GW "[gw]"

// MQTT
// la lib è PubSubClient (https://pubsubclient.knolleary.net/)
#include <PubSubClient.h>  // mqtt
PubSubClient mqtt_client(espClient);
#define TOPIC "HackLabCormano/" // da decidere
#define DELAY_MQTT 1000
String mqtt_server = "atrent.it";
String nomeNodo;  //poi viene accodato il mac, cambiare?
long lastMsg = 0;
#define MSG_LEN 150
char msg[MSG_LEN];

// sensori, ripulire, mettere i propri
float humidity=10,temperature=22,fahreneit=34,hif=45,hic=67;
int tempSoglia=26;
int finestraIsteresi=2;




////////////////////////////////
void setup() {
    Serial.begin(115200);
    Serial.println("Booting...");

    //wifiAndMqttOn();

    mqtt_client.setServer(mqtt_server.c_str(), 1883); // 1883 è porta default MQTT

    //mqtt_client.setCallback(FUNZIONE IMPLEMENTATA DA ME);
    Serial.println("mqtt set");

    wifi_setup();

    //////////////////////////////////////
    // tranquillamente buttare, si può fissare nome nodo, così lo imposta usando il MAC address
    Serial.println("MAC address: ");
    WiFi.macAddress(mac);
    nomeNodo=TOPIC;
    Serial.print("MAC=");
    for (int i=0; i<6; i++) {
        nomeNodo += String(mac[i],HEX);
        Serial.print(mac[i],HEX);
        Serial.print(":");
    }
    Serial.println(nomeNodo);
    // fin qui
    //////////////////////////////////////

    Serial.println("Booted!");
}



////////////////////////////////////////////////
void loop() {

    //aggiornare valori sensori
    //...

    // eventuali azioni in funzione dei valori dei sensori
    //...

    long now = millis();
    // manda solo se è passato un po' di tempo dall'ultima volta
    if (now - lastMsg > DELAY_MQTT) {
        lastMsg = now;
        mqtt_send();
    }

}

////////////////////////////////////
// funzioni varie
void mqtt_reconnect() {
    // TODO: nr. tentativi???  per forza perche' se non trova mqtt server non fa piu' nulla

    // TODO: condizionare invio (altrove)

    // Loop until we're reconnected
    if (!mqtt_client.connected()) {

        Serial.print("Attempting MQTT connection...");
        // Attempt to connect
        if (mqtt_client.connect(nomeNodo.c_str())) {  // si connette al BROKER dichiarando "identità"
            Serial.println("connected");

            // Once connected, publish an announcement...
            mqtt_client.publish(nomeNodo.c_str(), "first msg.");

            // ... and resubscribe
            //mqtt_client.subscribe("TermuinatorConfig/#");


        } else {
            Serial.print("failed, rc=");
            Serial.print(mqtt_client.state());
            Serial.println(" skipping MQTT...");
            // Wait 5 seconds before retrying
            //delay(5000);
        }
    }
}

void mqtt_send() {
    if (WiFi.status() == WL_CONNECTED) {
        if (!mqtt_client.connected()) {
            mqtt_reconnect(); // PRIMO PASSO FONDAMENTALE
            Serial.println("loop...");
        }

        if (mqtt_client.connected()) {

            mqtt_client.loop();



            // trasformazione di vari valori in "stringone" JSON


			// vedere: http://www.nongnu.org/avr-libc/
            char str_temp[6];
            char str_hic[6];
            char str_humidity[6];
            dtostrf(temperature, 4, 2, str_temp);
            dtostrf(hic, 4, 2, str_hic);
            dtostrf(humidity, 4, 2, str_humidity);

            // modo "alla C" di creare stringhe contenenti valori, si chiama "templating", ad ogni %qualcosa viene sostituito un valore (vedere parametri dopo la stringa)
            snprintf (msg, MSG_LEN,
                      "temp:%s, soglia:%d, hum:%s, h_index:%s",
                      str_temp,tempSoglia,str_humidity,str_hic);

            // Serial.println(msg);

            if (mqtt_client.publish(nomeNodo.c_str(), msg)) {
                Serial.println("info: MQTT message succesfully published");


            } else {
                Serial.println("error: MQTT publishing error (connection error or message too large)");
            }
        }
    }
}







boolean wifi_setup() {
    delay(100);

    // We start by connecting to a WiFi network
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(ssid);

    WiFi.disconnect();
    WiFi.begin(ssid.c_str(), password.c_str());

    for (int i=0; (WiFi.status() != WL_CONNECTED) && (i < WIFI_TENTATIVI) && digitalRead(0)==HIGH; i++) {
        delay(500);
        Serial.print(".");
    }

    if(WiFi.status() == WL_CONNECTED) {
        Serial.println();
        Serial.println("WiFi connected");
        Serial.println("IP address: ");
        Serial.println(WiFi.localIP());

        gateway=WiFi.gatewayIP();

        // si può buttare, serviva come rete di sicurezza, usare il GW come broker
        if(mqtt_server.equals(USE_GW)) mqtt_server=String(gateway[0])+"."+String(gateway[1])+"."+String(gateway[2])+"."+String(gateway[3]);

        Serial.println(gateway);

        //char n[nodo.length()];
        //nomeNodo=n;
        //Serial.println(nodo);
        return true;
    } else {
        Serial.println("WiFi FAILED!");
        return false;
    }
}













/*
int wifiAndMqttOn() {
    Serial.println("entro in net services");
    if(wifi.startsWith("y")) {
        // wifi (dip. da parametri)
        if(wifi_setup()) {
            // DONE: deve funzionare anche senza broker, quindi check se bloccante!!! fatto, nel senso che abbiamo messo la reconnect monotentativo
            // mqtt (dip. da parametri)
            mqtt_client.setServer(mqtt_server.c_str(), 1883); //TODO: porta come config in file
            mqtt_client.setCallback(parseMQTTconfig);
            Serial.println("mqtt set");
            return 0;
        }
        Serial.println("mqtt NOT set");
        return -1;
    }
    else {
        Serial.print("wifi enabled? ");
        Serial.println(wifi);
        return -2;
    }
}
*/
