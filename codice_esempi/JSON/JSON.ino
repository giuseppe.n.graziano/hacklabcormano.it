// ArduinoJson - arduinojson.org
// Copyright Benoit Blanchon 2014-2017
// MIT License
//
// This example shows how to parse a JSON document in an HTTP response.
// It uses the Ethernet library, but can be easily adapter for Wifi.
//
// It performs a GET resquest on arduinojson.org/example.json
// Here is the expected response:
// {
//   "sensor": "gps",
//   "time": 1351824120,
//   "data": [
//     48.756080,
//     2.302038
//   ]
// }

#include <ArduinoJson.h>

#include <ESP8266WiFi.h>
#define WIFI_TENTATIVI 100
#define USE_GW "[gw]"

WiFiClient client;
IPAddress gateway;

//String wifi = "";
String ssid = "HackLabCormano";
String password = "...";

// https://frassino.comperio.it/kpi/clavis-csbno.json

void setup() {
    // Initialize Serial port
    Serial.begin(115200);
    while (!Serial) continue;

    // Initialize wifi
    wifi_setup();

    delay(1000);

}

void get() {

    Serial.println(F("Connecting..."));

    // Connect to HTTP server
    client.setTimeout(10000);
    if (!client.connect("frassino.comperio.it", 80)) {
        Serial.println(F("Connection failed"));
        return;
    }

    Serial.println(F("Connected!"));

    // Send HTTP request
    client.println(F("GET /kpi/clavis-csbno.json HTTP/1.0"));
    //client.println(F("Host: arduinojson.org"));
    client.println(F("Connection: close"));
    if (client.println() == 0) {
        Serial.println(F("Failed to send request"));
        return;
    }

    // Check HTTP status
    char status[32] = {0};
    client.readBytesUntil('\r', status, sizeof(status));
    if (strcmp(status, "HTTP/1.1 200 OK") != 0) {
        Serial.print(F("Unexpected response: "));
        Serial.println(status);
        return;
    }

    // Skip HTTP headers
    char endOfHeaders[] = "\r\n\r\n";
    if (!client.find(endOfHeaders)) {
        Serial.println(F("Invalid response"));
        return;
    }

    const size_t bufferSize = JSON_OBJECT_SIZE(6) + 120;
    DynamicJsonBuffer jsonBuffer(bufferSize);

    //const char* json = "{\"dbName\":\"clavis-csbno\",\"utenti\":\"327980\",\"prestiti\":\"158367\",\"prestiti_oggi\":\"2010\",\"utentiplus\":\"6731\",\"utenti_attivi\":\"89353\"}";
    //JsonObject& root = jsonBuffer.parseObject(json);

    // Parse JSON object
    JsonObject& root = jsonBuffer.parseObject(client);
    if (!root.success()) {
        Serial.println(F("Parsing failed!"));
        return;
    }

    const char* dbName = root["dbName"]; // "clavis-csbno"
    const char* utenti = root["utenti"]; // "327980"
    const char* prestiti = root["prestiti"]; // "158367"
    const char* prestiti_oggi = root["prestiti_oggi"]; // "2010"
    const char* utentiplus = root["utentiplus"]; // "6731"
    const char* utenti_attivi = root["utenti_attivi"]; // "89353"

    Serial.println(prestiti_oggi);

    // Disconnect
    client.stop();
}

void loop() {
    // not used in this example
    get();
    delay(2000);
}


boolean wifi_setup() {
    delay(100);

    // We start by connecting to a WiFi network
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(ssid);

    WiFi.disconnect();
    WiFi.begin(ssid.c_str(), password.c_str());

    for (int i=0; (WiFi.status() != WL_CONNECTED) && (i < WIFI_TENTATIVI) && digitalRead(0)==HIGH; i++) {
        delay(500);
        Serial.print(".");
    }

    if(WiFi.status() == WL_CONNECTED) {
        Serial.println();
        Serial.println("WiFi connected");
        Serial.println("IP address: ");
        Serial.println(WiFi.localIP());

        gateway=WiFi.gatewayIP();

        //if(mqtt_server.equals(USE_GW)) mqtt_server=String(gateway[0])+"."+String(gateway[1])+"."+String(gateway[2])+"."+String(gateway[3]);

        Serial.println(gateway);

        //char n[nodo.length()];
        //nomeNodo=n;
        //Serial.println(nodo);
        return true;
    } else {
        Serial.println("WiFi FAILED!");
        return false;
    }
}
